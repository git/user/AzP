# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{11..13} )
inherit python-single-r1 qmake-utils xdg-utils

DESCRIPTION="A general purpose tile map editor"
HOMEPAGE="https://www.mapeditor.org/"
SRC_URI="https://github.com/bjorn/tiled/archive/v${PV}/${P}.tar.gz"

LICENSE="BSD BSD-2 GPL-2+"
SLOT="0"
KEYWORDS="~amd64"
IUSE="examples qt5 python"

REQUIRED_USE="python? ( ${PYTHON_REQUIRED_USE} )"

RDEPEND="
	app-arch/zstd:=
	>=dev-qt/qtcore-5.15:5
	>=dev-qt/qtdbus-5.15:5
	>=dev-qt/qtdeclarative-5.15:5
	>=dev-qt/qtgui-5.15:5
	>=dev-qt/qtnetwork-5.15:5
	>=dev-qt/qtwidgets-5.15:5
	sys-libs/zlib
	python? ( ${PYTHON_DEPS} )
"
DEPEND="${RDEPEND}"
BDEPEND="
	dev-qt/linguist-tools:5
	virtual/pkgconfig
	dev-util/qbs
"

DOCS=( AUTHORS COPYING NEWS.md README.md )

pkg_setup() {
	use qt5 && python && python-single-r1_pkg_setup
}

src_prepare() {
	eapply "${FILESDIR}"/${PN}-1.9.2-set-plugin-path.patch
	eapply_user

	sed -e "s|__EBUILD_TILED_PLUGIN_DIR__|$(get_libdir)/tiled/plugins|g" -i src/libtiled/libtiled.qbs
}

src_configure() {
	# Setup toolchains
	qbs setup-toolchains --detect
	# Create a Qt5 profile for Qbs
	qbs setup-qt "$(qt5_get_bindir)/qmake" qt5
}

src_compile() {
	# The installDir is appended to installPrefix
	qbs build --no-install \
		config:release \
		profile:qt5 \
		qbs.installDir:$(get_libdir) \
		qbs.installPrefix:/usr
}

src_install() {
	LIBDIR=$(get_libdir)
	PREFIX="/usr"
	qbs install -v --no-build \
		config:release \
		--install-root ${D} \
		qbs.installDir:${LIBDIR} \
		qbs.installPrefix:${PREFIX}
	mkdir -p "${ED}${PREFIX}/${LIBDIR}"
	mv -v "${ED}${PREFIX}"/lib/* "${ED}${PREFIX}"/"${LIBDIR}"/
}

pkg_postinst() {
	xdg_icon_cache_update
	xdg_desktop_database_update
	xdg_mimeinfo_database_update
}

pkg_postrm() {
	xdg_icon_cache_update
	xdg_desktop_database_update
	xdg_mimeinfo_database_update
}
