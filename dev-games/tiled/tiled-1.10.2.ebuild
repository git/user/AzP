# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{11..13} )
inherit python-single-r1 qmake-utils xdg-utils

DESCRIPTION="A general purpose tile map editor"
HOMEPAGE="https://www.mapeditor.org/"
SRC_URI="https://github.com/bjorn/tiled/archive/v${PV}/${P}.tar.gz"

LICENSE="BSD BSD-2 GPL-2+"
SLOT="0"
KEYWORDS="~amd64"
IUSE="examples qt6 python"

REQUIRED_USE="python? ( ${PYTHON_REQUIRED_USE} )"

RDEPEND="
	app-arch/zstd:=
	dev-qt/qtbase[dbus,gui,network,widgets]
	dev-qt/qtdeclarative:6
	dev-qt/qtquickcontrols2
	dev-qt/qtsvg:6
	sys-libs/zlib
	python? ( ${PYTHON_DEPS} )
"
DEPEND="${RDEPEND}"
BDEPEND="
	dev-qt/linguist-tools:5
	virtual/pkgconfig
	dev-util/qbs
"

DOCS=( AUTHORS COPYING NEWS.md README.md )

pkg_setup() {
	use qt6 && python && python-single-r1_pkg_setup
}

src_prepare() {
	eapply_user

	sed -e "s|__EBUILD_TILED_PLUGIN_DIR__|$(get_libdir)/tiled/plugins|g" -i src/libtiled/libtiled.qbs
}

src_configure() {
	# Setup toolchains
	qbs setup-toolchains --detect
	# Create a Qt6 profile for Qbs
	qbs setup-qt "$(qt6_get_bindir)/qmake6" qt6
	qbs config profiles.qt6.baseProfile x86_64-pc-linux-gnu-gcc-14
	qbs config defaultProfile qt6
}

src_compile() {
	# The installDir is appended to installPrefix
	qbs build --no-install \
		config:release \
		profile:qt6 \
		project.libDir:lib64 \
		qbs.installDir:$(get_libdir) \
		qbs.installPrefix:"/usr" \
		projects.Tiled.useRPaths:false \
		projects.Tiled.installHeaders:true
}

src_install() {
	LIBDIR=$(get_libdir)
	PREFIX="/usr"
	qbs install -v --no-build \
		config:release \
		--install-root ${D} \
		qbs.installDir:${LIBDIR} \
		qbs.installPrefix:${PREFIX}
	mkdir -p "${ED}${PREFIX}/${LIBDIR}"
	mv -v "${ED}${PREFIX}"/lib/* "${ED}${PREFIX}"/"${LIBDIR}"/
}

pkg_postinst() {
	xdg_icon_cache_update
	xdg_desktop_database_update
	xdg_mimeinfo_database_update
}

pkg_postrm() {
	xdg_icon_cache_update
	xdg_desktop_database_update
	xdg_mimeinfo_database_update
}
