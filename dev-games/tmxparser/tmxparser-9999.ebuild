# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

# The swig fork is required for compatibility with both provided and
# 3rd-party Python scripts.  Required patch was sent to upstream in
# 2014: https://github.com/swig/swig/pull/251

DOCS_BUILDER="doxygen"
inherit cmake docs git-r3

DESCRIPTION="A library for parsing tmx files"
HOMEPAGE="https://github.com/sainteos/tmxparser.git"
SRC_URI="https://github.com/sainteos/tmxparser.git"
EGIT_REPO_URI="https://github.com/sainteos/tmxparser.git"

LICENSE="MIT"
SLOT="0"
IUSE="miniz test"
REQUIRED_USE=""
RESTRICT="!test? ( test )"

# Uses internal header-only miniz.c
RDEPEND="
	!miniz? ( sys-libs/zlib )
	>=dev-libs/tinyxml2-6.0.0
"
DEPEND="${RDEPEND}"
BDEPEND=""

PATCHES=(
	"${FILESDIR}"/${PN}-replace-deprecated-cpp.patch
	"${FILESDIR}"/${PN}-fix-build-flags.patch
)

src_configure() {
	local mycmakeargs=(
		-DUSE_MINIZ=$(use miniz)
		-DBUILD_SHARED_LIBS=ON
		-DBUILD_TINYXML2=OFF
		-DBUILD_TESTS=$(use test)
		-DBUILD_DOCS=$(use doc)
	)

	cmake_src_configure
}

src_compile() {
	cmake_src_compile

	# Automatically exits if useflag docs is not set
	docs_compile
}
