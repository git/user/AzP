# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake

S=${WORKDIR}/DevIL-${PV}/DevIL/

DESCRIPTION="DevIL image library"
HOMEPAGE="http://openil.sourceforge.net/"
SRC_URI="https://github.com/DentonW/DevIL/archive/v${PV}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64 ~hppa ~mips ~ppc ~ppc64 ~x86"
IUSE="lcms jpeg mng openexr opengl png sdl test tiff X xpm"

PATCHES=(
	"${FILESDIR}/${PN}-cmake-install-paths.patch"
)

RESTRICT="!test? ( test )"

RDEPEND="
	jpeg? ( virtual/jpeg )
	mng? ( media-libs/libmng:= )
	openexr? ( media-libs/openexr:= )
	opengl? ( virtual/opengl
			virtual/glu )
	png? ( media-libs/libpng:0= )
	sdl? ( media-libs/libsdl )
	tiff? ( media-libs/tiff:0 )
	X? ( x11-libs/libXext
		 x11-libs/libX11
		 x11-libs/libXrender )
	xpm? ( x11-libs/libXpm )"
DEPEND="${RDEPEND}
	virtual/pkgconfig
	X? ( x11-base/xorg-proto )"

src_configure() {
	local mycmakeargs=(
		-DIL_MNG_LIB="$(usex mng)"
		-DIL_NO_JPG="$(usex jpeg)"
		-DIL_NO_LCMS="$(usex lcms)"
		-DIL_NO_PNG="$(usex png)"
		-DIL_NO_TIF="$(usex tiff)"
		-DIL_USE_DXTC_SQUISH="false"
		-DIL_TESTS="$(usex test)"
		#-DIL_JASPER_LIB="$(usex jasper)"
		#-DIL_JPEG_LIB="$(usex jpeg)"
		#-DIL_LCMS2_LIB="$(usex lcms2)"
		#-DIL_NVTT_LIB="$(usex nvtt)"
		#-DIL_OPENEXR_LIB="$(usex openexr)"
		#-DIL_PNG_LIB="$(usex png)"
		#-DIL_SQUISH_LIB="false"
		#-DIL_TIFF_LIB="$(usex tiff)"
		-DLCMS_NODIRINCLUDE="true"
	)
	cmake_src_configure
}

src_install() {
	cmake_src_install

	# package provides .pc files
	find "${D}" -name '*.la' -delete || die
}
